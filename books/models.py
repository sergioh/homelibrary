from django.db import models
from datetime import datetime
from django.conf import settings

class Author(models.Model):
    name = models.CharField(max_length=30)
    lastname = models.CharField(max_length=40)

    class Meta:
        ordering = ['lastname']

    def __str__(self):
        return str(self.lastname + ',' +self.name)

class Category(models.Model):
    name = models.CharField(max_length=30)

    def __str__(self):
        return str(self.name)


class Book(models.Model):
    bookid = models.AutoField(primary_key=True)
    user = models.ForeignKey(
        settings.AUTH_USER_MODEL,
        on_delete=models.CASCADE,
    )
    isbn = models.CharField(max_length=13, null=True, blank=True )
    title = models.CharField(max_length=500, null=True)
    author = models.ManyToManyField(Author)
    category = models.ManyToManyField(Category)
    editorial = models.CharField(max_length=100, null=True, blank=True)
    binding = models.CharField(max_length=13, null=True, blank=True )
    collection = models.CharField(max_length=500, null=True, blank=True)
    summary = models.TextField(null=True, blank=True)
    date = models.IntegerField(null=True, blank=True)
    bookshelf = models.CharField(max_length=100,null=True, blank=True)
    shelf = models.CharField(max_length=100,null=True, blank=True)
    available = models.BooleanField(default=True, null=False)

    def authors(self):
        return "\n".join([a.lastname+', '+a.name for a in self.author.all()])

    def categories(self):
        return "\n".join([c.name for c in self.category.all()])

    def __str__(self):
        return str(self.title)
