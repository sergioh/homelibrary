#use: ./manage.py remove_orphan_authors
from django.core.management.base import BaseCommand, CommandError

#import models
from books.models import Author

class Command(BaseCommand):

    def handle(self, **options):
      for author in Author.objects.all():
        if not author.book_set.select_related():
          print ('orphan: ' + author.name)
          author.delete()
        else:
          print ('not orphan: ' + author.name)
