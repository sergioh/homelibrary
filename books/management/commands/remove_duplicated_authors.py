#use: ./manage.py remove_duplicated_authors
from django.core.management.base import BaseCommand, CommandError

#import models
from books.models import Book
from books.models import Author

class Command(BaseCommand):

    def handle(self, **options):
      #get all authors
      allauthors = Author.objects.filter()
      for allauthor in allauthors:
        print (allauthor.id)
        print (allauthor.name)
        print (allauthor.lastname)
        #get one author object by name, lastname
        authors = Author.objects.filter(name=allauthor.name,lastname=allauthor.lastname).order_by('id')[:1]
        for author in authors:
          print (author.id)
          print (author.name)
          print (author.lastname)
          #get all books assigned to this author by name, lastname
          books = Book.objects.filter(author__name=author.name,author__lastname=author.lastname)
          for book in books:
            #clear author and assign the same author object to all books
            print (book.bookid)
            book.author.clear()
            book.author.add(author)
            book.save()
