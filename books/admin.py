from django.contrib import admin

# Register your models here.
from .models import Book, Author, Category

class BookAdmin(admin.ModelAdmin):
    list_display = ('title', 'isbn', 'authors', 'categories', 'bookshelf', 'shelf')
    # show only user books in admin
    def get_queryset(self, request):
        qs = super(BookAdmin, self).get_queryset(request)
        if request.user.is_superuser:
            return qs
        return qs.filter(user=request.user)
    # assign logged in user to book
    def save_model(self, request, obj, form, change):
        obj.user = request.user
        super().save_model(request, obj, form, change)
    # exclude user field in book form
    def get_fields(self, request, obj=None):
        fields = super(BookAdmin, self).get_fields(request, obj)
        fields.remove('user')
        return fields


class AuthorAdmin(admin.ModelAdmin):
    list_display = ('lastname', 'name')

class CategoryAdmin(admin.ModelAdmin):
    list_display = ('name',)

admin.site.register(Book, BookAdmin)
admin.site.register(Author, AuthorAdmin)
admin.site.register(Category, CategoryAdmin)
